const Statistics = require("../models/statistics.model");
const Order = require("../models/order.model");
const Customer = require("../models/customer.model");
const Product = require("../models/product.model");
const SiteVisit = require("../models/siteVisit.model");
const moment = require("moment");

exports.getAll = async (req, res) => {
  try {
    const statistics = await Statistics.find({});
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: statistics[0],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getByOption = async (req, res) => {
  const { option } = req.query;

  try {
    const statistics = await Statistics.find({});
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: statistics[0][option],
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.increaseSiteVisit = async (req, res) => {
  try {
    let log = {};
    const siteVisit = await SiteVisit.findOne({
      date: moment().utcOffset("+07:00").format("DD-MM-YYYY"),
    });
    if (!siteVisit) {
      const temp = new SiteVisit({
        date: moment().utcOffset("+07:00").format("DD-MM-YYYY"),
        quantity: 1,
      });
      await temp.save();
      log = {
        date: moment().utcOffset("+07:00").format("DD-MM-YYYY"),
        quantity: 1,
      };
    } else {
      log = await SiteVisit.findByIdAndUpdate(
        siteVisit._id,
        { $inc: { quantity: 1 } },
        { new: true }
      );
    }

    const statistics = await Statistics.find({});
    const statisticsId = statistics[0]._id;
    const newStatistics = await Statistics.findByIdAndUpdate(
      statisticsId,
      { $inc: { totalVisitsSite: 1 } },
      { new: true }
    );

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: { totalVisitsSite: newStatistics.totalVisitsSite, log },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getSalesOfWeek = async (req, res) => {
  try {
    const temp = await Promise.all([
      this.calcSalesOfWeek(
        moment().subtract(6, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcSalesOfWeek(
        moment().subtract(5, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcSalesOfWeek(
        moment().subtract(4, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcSalesOfWeek(
        moment().subtract(3, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcSalesOfWeek(
        moment().subtract(2, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcSalesOfWeek(
        moment().subtract(1, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcSalesOfWeek(moment().utcOffset("+07:00").format("DD-MM-YYYY")),
    ]);

    const sales = temp.map((e) => {
      if (e.length === 0) return 0;
      else return e[0].sales;
    });

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: sales,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.calcSalesOfWeek = async (date) => {
  return Order.aggregate([
    {
      $match: {
        status: "verified",
        "cart.saleDate": date,
      },
    },
    { $unwind: "$cart.info" },
    {
      $group: {
        _id: null,
        date: { $first: "$cart.saleDate" },
        sales: { $sum: "$cart.info.quantity" },
      },
    },
  ]);
};

exports.getOrdersOfWeek = async (req, res) => {
  try {
    const temp = await Promise.all([
      this.calcOrdersOfWeek(
        moment().subtract(6, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcOrdersOfWeek(
        moment().subtract(5, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcOrdersOfWeek(
        moment().subtract(4, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcOrdersOfWeek(
        moment().subtract(3, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcOrdersOfWeek(
        moment().subtract(2, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcOrdersOfWeek(
        moment().subtract(1, "days").utcOffset("+07:00").format("DD-MM-YYYY")
      ),
      this.calcOrdersOfWeek(moment().utcOffset("+07:00").format("DD-MM-YYYY")),
    ]);

    const orders = temp.map((e) => {
      if (e.length === 0) return 0;
      else return e[0].orders;
    });

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: orders,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.calcOrdersOfWeek = (date) => {
  return Order.aggregate([
    {
      $match: {
        "cart.orderDate": date,
      },
    },
    { $unwind: "$cart.info" },
    {
      $group: {
        _id: null,
        date: { $first: "$cart.orderDate" },
        orders: { $sum: "$cart.info.quantity" },
      },
    },
  ]);
};

exports.init = async (req, res) => {
  try {
    let totalSales = 0;
    let totalSalesMoney = 0;
    const sales = await Order.find({ status: "verified" });
    for (let sale of sales) {
      totalSales += sale.cart.info.length;
      totalSalesMoney += sale.cart.totalPrice;
    }

    let totalOrders = 0;
    const orders = await Order.find({});
    for (let order of orders) {
      totalOrders += order.cart.info.length;
    }

    const totalCustomers = await Customer.countDocuments();

    const totalVietnameseBooks = await Product.countDocuments({
      type: "VietNameseBook",
    });
    const totalForeignBooks = await Product.countDocuments({
      type: "ForeignBook",
    });
    const totalStationeries = await Product.countDocuments({
      type: "Stationery",
    });
    const totalSouvenirs = await Product.countDocuments({ type: "Souvenir" });
    const totalEbooks = await Product.countDocuments({ type: "Ebook" });
    const statisticsInit = new Statistics({
      totalSales,
      totalSalesMoney,
      totalOrders,
      totalCustomers,
      totalVietnameseBooks,
      totalForeignBooks,
      totalStationeries,
      totalSouvenirs,
      totalEbooks,
    });
    const newStatistics = await statisticsInit.save();

    return res.status(200).json({
      success: true,
      message: "Khởi tạo thông tin thành công",
      data: newStatistics,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    const statistics = await Statistics.deleteMany();
    if (statistics.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "Không có thống kê nào" });
    }

    return res.status(200).json({
      success: true,
      message: "Xóa thống kê thành công",
      data: statistics,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
