require("dotenv").config();

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user.model");

const saltRounds = parseInt(process.env.SALT_ROUNDS_BCRYPT) || 10;
const secretToken = process.env.SECRET_TOKEN;

exports.register = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: "Thiếu tên tài khoản hoặc mật khẩu" });
  }

  try {
    const user = await User.findOne({ username });

    if (user) {
      return res
        .status(400)
        .json({ success: false, message: "Tên tài khoản đã tồn tại" });
    }

    const salt = await bcrypt.genSalt(saltRounds);
    const hashedPassword = await bcrypt.hash(password, salt);
    const newUser = new User({ ...req.body, password: hashedPassword });
    await newUser.save();
    delete newUser.password;

    return res.status(200).json({
      success: true,
      message: `Đăng kí tài khoản ${newUser.username} thành công!`,
      data: newUser,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.login = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: "Thiếu tên tài khoản hoặc mật khẩu" });
  }

  try {
    const user = await User.findOne({ username });
    if (!user) {
      return res
        .status(400)
        .json({ success: false, message: "Sai tài khoản hoặc mật khẩu" });
    }

    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) {
      return res
        .status(400)
        .json({ success: false, message: "Sai tài khoản hoặc mật khẩu" });
    }

    const token = jwt.sign(
      { userId: user._id, userRole: user.role },
      secretToken
    );

    return res.status(200).json({
      success: true,
      message: "Đăng nhập thành công!",
      token,
      data: user,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.logout = (req, res) => {
  return res.status(200).json({ success: true, message: "Bạn đã đăng xuất" });
};

exports.updateSelf = async (req, res) => {
  const userId = req.user?.userId;

  try {
    const user = await User.findByIdAndUpdate(userId, req.body, { new: true });
    return res.status(200).json({
      success: true,
      message: "Cập nhật thành công",
      data: {
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        country: user.country,
        address: user.address,
        postcode: user.postcode,
        phone: user.phone,
        email: user.email,
        role: user.role,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.update = async (req, res) => {
  const { email } = req.body;

  delete req.body.username;
  delete req.body.password;

  if (!email) {
    return res.status(400).json({ success: false, message: "Phải có email" });
  }

  try {
    if (email) {
      const user = await User.find({
        email,
        _id: { $ne: req.params.id },
      });
      if (user.length > 0) {
        return res
          .status(400)
          .json({ success: false, message: "Email đã được dùng" });
      }
    }

    const user = await User.findOneAndUpdate({ _id: req.params.id }, req.body, {
      new: true,
    }).select("-password");
    if (!user) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy user" });
    }

    return res.status(200).json({
      success: true,
      message: `Cập nhật user ${user.username} thành công`,
      data: user,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getUserSelf = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    console.log(user);
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        company: user.company,
        country: user.country,
        address: user.address,
        postcode: user.postcode,
        phone: user.phone,
        email: user.email,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getById = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        company: user.company,
        country: user.country,
        address: user.address,
        postcode: user.postcode,
        phone: user.phone,
        email: user.email,
        role: user.role,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getByOption = async (req, res) => {
  let { sort, sortName, keyword, keywordAllField } = req.query;
  let keywordReg, keywordAllFieldReg;
  if (keyword) keywordReg = new RegExp(keyword, "i");

  const page = parseInt(req.query.page) || 1;
  const perPage = parseInt(req.query.perPage) || 12;
  const start = (page - 1) * perPage;
  const end = page * perPage;

  try {
    let obj = { keywordReg };
    let query = {};
    let user = [];
    for (let key in obj) {
      if (obj[key]) query[key] = obj[key];
    }
    console.log(query);

    if (keywordAllField) {
      keywordAllField = decodeURIComponent(
        keywordAllField.replace(/%jsfei026/g, "&")
      );
      keywordAllFieldReg = new RegExp(keywordAllField, "i");

      user = await User.find({
        $or: [
          { username: keywordAllFieldReg },
          { firstName: keywordAllFieldReg },
          { lastName: keywordAllFieldReg },
          { email: keywordAllFieldReg },
          { phone: keywordAllFieldReg },
          { email: keywordAllFieldReg },
          { country: keywordAllFieldReg },
          { address: keywordAllFieldReg },
          { postcode: keywordAllField },
        ],
      });
    } else {
      user = await User.find(query)
        .sort({ [sortName]: sort })
        .select("-password");
    }

    if (user.length === 0)
      return res.status(400).json({
        success: false,
        message: "Không có nhân viên nào",
        data: {},
      });

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        category: user.reverse().slice(start, end),
        totalPages: Math.ceil(user.length / perPage),
        totalUsers: user.length,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.delete = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete({ _id: req.params.id });
    if (!user) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy nhân viên" });
    }

    return res.status(200).json({
      success: true,
      message: `Xóa nhân viên ${user.username} thành công`,
      data: user,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    const user = await User.deleteMany();
    if (user.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "Không có nhân viên nào" });
    }

    return res.status(200).json({
      success: true,
      message: "Xóa tất cả nhân viên thành công",
      data: user,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
