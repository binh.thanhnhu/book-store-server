const Product = require("../models/product.model");
const Statistics = require("../models/statistics.model");
const { cloudinary } = require("../utils");
const {
  getBestSeller,
  getNewest,
  getDiscount,
  getHighlightMonth,
} = require("./category.controller");

exports.create = async (req, res) => {
  const { SKU } = req.body;
  delete req.body.salePrice;
  delete req.body.tempPrice;

  const typeMap = {
    VietNameseBook: "totalVietnameseBooks",
    ForeignBook: "totalForeignBooks",
    Stationery: "totalStationeries",
    Souvenir: "totalSouvenirs",
  };

  try {
    const sku = SKU ? await Product.findOne({ SKU }) : SKU;
    if (sku) {
      return res
        .status(400)
        .json({ success: false, message: "Trùng mã sản phẩm (SKU)" });
    }

    const fileStr = req.body.image;

    if (fileStr.length > 5)
      return res
        .status(400)
        .json({ success: false, message: "Chỉ upload tối đa được 5 ảnh" });

    let size = 0;
    let isImage = true;
    for (let i = 0; i < fileStr.length; i++) {
      if (
        fileStr[i].info.fileType !== "image/jpeg" &&
        fileStr[i].info.fileType !== "image/png"
      ) {
        isImage = false;
        break;
      }
      size += fileStr[i].info.fileSize;
    }

    if (!isImage)
      return res
        .status(400)
        .json({ success: false, message: "Không đúng định dạng ảnh" });

    if (size > 2 * 1024 * 1024) {
      return res
        .status(400)
        .json({ success: false, message: "Dung lượng file quá lớn" });
    }

    let multipleUploadPromise = fileStr.map((file) =>
      cloudinary.uploader.upload(file.source, {
        upload_preset: "book-store",
      })
    );

    const uploadResponse = await Promise.all(multipleUploadPromise);

    const arr = [];
    for (let i = 0; i < uploadResponse.length; i++) {
      const temp = {};
      temp.public_id = uploadResponse[i].public_id;
      temp.url = uploadResponse[i].url;
      temp.fileSize = uploadResponse[i].bytes;
      arr.push(temp);
    }

    const newProduct = new Product(req.body);
    newProduct.imageFromUrl.push(...arr);
    await newProduct.save();

    await Statistics.findOneAndUpdate(typeMap[product.type], {
      $inc: { [typeMap[product.type]]: 1 },
    });

    return res.status(200).json({
      success: true,
      message: `Tạo mới sản phẩm ${newProduct.SKU} thành công!`,
      data: newProduct,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.createMany = async (req, res) => {
  const typeMap = {
    VietNameseBook: "totalVietnameseBooks",
    ForeignBook: "totalForeignBooks",
    Stationery: "totalStationeries",
    Souvenir: "totalSouvenirs",
  };

  try {
    const data = await Product.insertMany(req.body);

    let promise = [];
    for (product of data) {
      const temp = Statistics.findOneAndUpdate(typeMap[product.type], {
        $inc: { [typeMap[product.type]]: 1 },
      });
      promise.push(temp);
    }

    await Promise.all(promise);

    if (data.length === 0)
      return res
        .status(400)
        .json({ success: false, message: "Không thể thêm mới" });
    else
      res
        .status(200)
        .json({ success: true, message: "Tạo mới thành công", data });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getById = async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);
    if (!product) {
      return res.status(400).json({
        success: false,
        message: "Không có sản phẩm để hiển thị",
        data: product,
      });
    }

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: product,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message:
        error.kind === "ObjectId"
          ? "Không có sản phẩm để hiển thị"
          : "Internal server error",
    });
  }
};

exports.getAll = async (req, res) => {
  try {
    const products = await Product.find({});

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: products,
    });
    //
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getByOption = async (req, res) => {
  let { type, option, sort, sortName, categories, keyword, keywordAllField } =
    req.query;
  let keywordReg, keywordAllFieldReg;
  if (keyword) keywordReg = new RegExp(keyword, "i");

  const page = parseInt(req.query.page) || 1;
  const perPage = parseInt(req.query.perPage) || 12;
  const start = (page - 1) * perPage;
  const end = page * perPage;

  const TypeProduct = [
    { value: "VietNameseBook", label: "Sách Việt Nam" },
    { value: "ForeignBook", label: "Sách nước ngoài" },
  ];

  const OptionVietNameseBook = [
    { value: "Literature", label: "Sách văn học" },
    { value: "Economic", label: "Sách kinh tế" },
    { value: "ChildrensBooks", label: "Sách cho trẻ em" },
    { value: "LifeSkill", label: "Sách kỹ năng sống" },
    { value: "Textbook", label: "Sách giáo khoa" },
    { value: "Comic", label: "Truyện tranh" },
    { value: "ReligionAndSpirituality", label: "Sách tôn giáo - tâm linh" },
    { value: "Journal", label: "Tạp chí" },
  ];

  const OptionForeignBook = [
    { value: "Cookbooks", label: "Cookbooks" },
    { value: "ScienceAndTechnology", label: "Science & Technology" },
    { value: "ParentingAndRelationships", label: "Parenting & Relationships" },
    { value: "Magazines", label: "Magazines" },
    { value: "Dictionary", label: "Dictionary" },
    { value: "SelfHelp", label: "Self Help" },
    { value: "ArtAndPhotography", label: "Art & Photography" },
    { value: "ChildrensBooks", label: "Children's Books" },
  ];

  if (keywordAllField) {
    keywordAllField = decodeURIComponent(
      keywordAllField.replace(/%jsfei026/g, "&")
    );
    keywordAllFieldReg = new RegExp(keywordAllField, "i");

    keywordAllFieldType = [];
    keywordAllFieldOption = [];
    TypeProduct.forEach((item) => {
      if (item.label.match(keywordAllFieldReg)?.length > 0)
        keywordAllFieldType.push(item.value);
    });
    OptionVietNameseBook.forEach((item) => {
      if (item.label.match(keywordAllFieldReg)?.length > 0)
        keywordAllFieldOption.push(item.value);
    });
    OptionForeignBook.forEach((item) => {
      if (item.label.match(keywordAllFieldReg)?.length > 0)
        keywordAllFieldOption.push(item.value);
    });
  }

  try {
    let obj = { type, option, categories, title: keywordReg };
    let query = {};
    let product = [];
    for (let key in obj) {
      if (obj[key]) query[key] = obj[key];
    }
    console.log(query);

    if (keywordAllField) {
      product = await Product.find({
        $or: [
          { title: keywordAllFieldReg },
          { author: keywordAllFieldReg },
          { SKU: keywordAllFieldReg },
          { type: keywordAllFieldType },
          { option: keywordAllFieldOption },
          { publishingCompany: keywordAllFieldReg },
        ],
      });
    } else {
      product = await Product.find(query).sort({ [sortName]: sort });
    }

    if (product.length === 0)
      return res.status(400).json({
        success: false,
        message: "Không có sản phẩm nào",
        data: {},
      });

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        category: product.reverse().slice(start, end),
        totalPages: Math.ceil(product.length / perPage),
        totalProducts: product.length,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getByCategory = async (req, res) => {
  const { category } = req.query;
  switch (category) {
    case "BestSeller":
      getBestSeller(req, res);
      break;
    case "Newest":
      getNewest(req, res);
      break;
    case "Discount":
      getDiscount(req, res);
      break;
    case "HighlightMonth":
      getHighlightMonth(req, res);
      break;
    default:
      return null;
  }
};

exports.update = async (req, res) => {
  const { userId, userRole } = req.user;
  const { title, SKU } = req.body;

  if (!title) {
    return res
      .status(400)
      .json({ success: false, message: "Phải có tên sách" });
  }

  try {
    const product = await Product.findById(req.params.id);

    if (!product) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy sản phẩm" });
    }

    if (product.creatorId !== userId && userRole !== "admin") {
      return res
        .status(400)
        .json({ success: false, message: "Không có quyền cập nhật" });
    }

    if (SKU) {
      const productSKU = await Product.find({
        SKU,
        _id: { $ne: req.params.id },
      });
      if (productSKU.length > 0) {
        return res
          .status(400)
          .json({ success: false, message: "Trùng mã sản phẩm" });
      }
    }

    const fileStr = req.body.image;
    const { imagesDelete } = req.body;
    let { imageFromUrl } = product;
    if (imagesDelete && imagesDelete.length > 0) {
      imageFromUrl = product.imageFromUrl.filter(
        (e) => !imagesDelete.includes(e.public_id)
      );
    }

    const arr = [];

    if (fileStr && fileStr.length > 0) {
      if (fileStr.length + imageFromUrl.length > 5)
        return res
          .status(400)
          .json({ success: false, message: "Chỉ upload tối đa được 5 ảnh" });

      let sizeFromFileStr = 0;
      let isImage = true;
      for (let i = 0; i < fileStr.length; i++) {
        if (
          fileStr[i].info.fileType !== "image/jpeg" &&
          fileStr[i].info.fileType !== "image/png"
        ) {
          isImage = false;
          break;
        }
        sizeFromFileStr += fileStr[i].info.fileSize;
      }

      let sizeFromImageFromUrl = 0;
      if (imageFromUrl && imageFromUrl.length > 0) {
        for (let i = 0; i < imageFromUrl.length; i++) {
          sizeFromImageFromUrl += imageFromUrl[i].fileSize;
        }
      }

      if (!isImage)
        return res
          .status(400)
          .json({ success: false, message: "Không đúng định dạng ảnh" });

      if (sizeFromFileStr + sizeFromImageFromUrl > 2 * 1024 * 1024) {
        return res
          .status(400)
          .json({ success: false, message: "Chỉ upload tối đa 2MB" });
      }

      let multipleUploadPromise = fileStr.map((file) =>
        cloudinary.uploader.upload(file.source, {
          upload_preset: "book-store",
        })
      );
      const uploadResponse = await Promise.all(multipleUploadPromise);
      for (let i = 0; i < uploadResponse.length; i++) {
        const temp = {};
        temp.public_id = uploadResponse[i].public_id;
        temp.url = uploadResponse[i].url;
        temp.fileSize = uploadResponse[i].bytes;
        arr.push(temp);
      }
    }

    updatedProduct = await Product.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      {
        new: true,
      }
    );

    if (imagesDelete.length > 0) {
      let multipleDeletePromise = imagesDelete.map((public_id) =>
        cloudinary.uploader.destroy(public_id)
      );
      await Promise.all(multipleDeletePromise);
      const imageFromUrl = updatedProduct.imageFromUrl.filter(
        (e) => !imagesDelete.includes(e.public_id)
      );

      await Product.findOneAndUpdate(
        { _id: req.params.id },
        { imageFromUrl },
        {
          new: true,
        }
      );
    }

    if (fileStr && fileStr.length > 0) {
      updatedProduct.imageFromUrl.push(...arr);
      await updatedProduct.save();
    }

    return res.status(200).json({
      success: true,
      message: `Cập nhật sản phẩm ${product.SKU} thành công`,
      data: product,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.delete = async (req, res) => {
  const { userId, userRole } = req.user;
  const typeMap = {
    VietNameseBook: "totalVietnameseBooks",
    ForeignBook: "totalForeignBooks",
    Stationery: "totalStationeries",
    Souvenir: "totalSouvenirs",
  };

  try {
    const product = await Product.findById(req.params.id);

    if (!product) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy sản phẩm" });
    }

    if (product.creatorId !== userId && userRole !== "admin") {
      return res
        .status(400)
        .json({ success: false, message: "Không có quyền xóa" });
    }
    await Product.findByIdAndDelete({ _id: req.params.id });

    await Statistics.findOneAndUpdate(typeMap[product.type], {
      $inc: { [typeMap[product.type]]: -1 },
    });

    return res.status(200).json({
      success: true,
      message: `Xóa sản phẩm ${product.SKU} thành công`,
      data: product,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    const product = await Product.deleteMany();
    if (product.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "Không có sản phẩm nào" });
    }

    return res.status(200).json({
      success: true,
      message: "Xóa sản phẩm thành công",
      data: product,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
