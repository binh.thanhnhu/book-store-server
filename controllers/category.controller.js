const BestSeller = require("../models/bestSeller.model");
const Product = require("../models/product.model");

exports.getBestSeller = async (req, res) => {
  const limit = parseInt(req.query.limit) || 8;
  const bestSeller = await BestSeller.find({}).sort({ quantity: -1 });
  const values = [];
  let i = 0;
  for (let j = 0; j < bestSeller.length; j++) {
    const product = await Product.findOne({ SKU: bestSeller[j].SKU });
    if (product) {
      values.push(product);
      i++;
    }
    if (i === limit) break;
  }

  return res.status(200).json({
    success: true,
    message: "Lấy thông tin thành công",
    data: values,
  });
};

exports.getNewest = async (req, res) => {
  const limit = parseInt(req.query.limit) || 5;
  const values = await Product.find({}).sort({ createdAt: -1 }).limit(limit);

  return res.status(200).json({
    success: true,
    message: "Lấy thông tin thành công",
    data: values,
  });
};

exports.getDiscount = async (req, res) => {
  const limit = parseInt(req.query.limit) || 5;
  const values = await Product.find({
    oldPrice: { $ne: null },
  }).limit(limit);

  return res.status(200).json({
    success: true,
    message: "Lấy thông tin thành công",
    data: values,
  });
};

exports.getHighlightMonth = async (req, res) => {
  const limit = parseInt(req.query.limit) || 8;
  const date = new Date();
  const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 1);
  const values = await Product.find({
    createdAt: { $gte: firstDay, $lt: lastDay },
  }).limit(limit);

  return res.status(200).json({
    success: true,
    message: "Lấy thông tin thành công",
    data: values,
  });
};
