const Order = require("../models/order.model");
const Customer = require("../models/customer.model");
const Product = require("../models/product.model");
const BestSeller = require("../models/bestSeller.model");
const Message = require("../models/message.model");
const Statistics = require("../models/statistics.model");
const moment = require("moment");

exports.checkout = async (req, res) => {
  const { details, products } = req.body;
  const customerId = req.user?.customerId;

  try {
    const customer = await Customer.findById(customerId).select("-password");
    if (!customer) {
      return res.status(400).json({
        success: false,
        message: "Không tìm thấy khách hàng",
      });
    }

    if (!customer.email && !details.email) {
      return res.status(400).json({
        success: false,
        message: "Phải có email",
      });
    }

    const data = { ...details };
    data.cart = {};
    data.cart = products;

    if (customerId) data.customerId = customerId;
    const newOrder = new Order(data);
    const order = await newOrder.save();

    console.log("hihi", products.info.length);

    const statistics = await Statistics.find({});
    const statisticsId = statistics[0]._id;
    await Statistics.findByIdAndUpdate(statisticsId, {
      $inc: { totalOrders: products.info.length },
    });

    const newMessage = new Message({
      message: `Đơn hàng của ${customer.username} (${
        customer.email || details.email
      }) cần được xử lý`,
      option: "info",
      route: "order",
      suffix: order._id,
    });
    await newMessage.save();

    return res.status(200).json({
      success: true,
      message: "Đặt hàng thành công, đơn hàng sẽ chờ được xử lý",
      data: newOrder,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.orderHistorySelf = async (req, res) => {
  const customerId = req.user?.customerId;

  try {
    const listOrder = await Order.find({ customerId });
    if (listOrder.length === 0)
      return res.status(200).json({
        success: true,
        message: "Không có lịch sử giao dịch",
      });
    return res.status(200).json({
      success: true,
      message: "",
      data: listOrder.map((obj) => obj.cart).reverse(),
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getAll = async (req, res) => {
  try {
    const orders = await Order.find({});
    return res.status(200).json({
      success: true,
      message: "",
      data: orders,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getById = async (req, res) => {
  try {
    const order = await Order.findById(req.params.id);
    const products = order.cart.info;
    const data = JSON.parse(JSON.stringify(order));

    for (let i = 0; i < products.length; i++) {
      const product = await Product.findById(products[i].productId);
      data.cart.info[i].SKU = product.SKU;
      data.cart.info[i].productTitle = product.title;
    }

    if (!order) {
      return res.status(400).json({
        success: false,
        message: "Đường dẫn sai",
      });
    }

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getByOption = async (req, res) => {
  let { sort, sortName, keyword, keywordAllField } = req.query;
  let keywordReg, keywordAllFieldReg;
  if (keyword) keywordReg = new RegExp(keyword, "i");

  const page = parseInt(req.query.page) || 1;
  const perPage = parseInt(req.query.perPage) || 12;
  const start = (page - 1) * perPage;
  const end = page * perPage;

  try {
    let obj = { keywordReg };
    let query = {};
    let order = [];
    for (let key in obj) {
      if (obj[key]) query[key] = obj[key];
    }
    console.log(query);

    if (keywordAllField) {
      keywordAllField = decodeURIComponent(
        keywordAllField.replace(/%jsfei026/g, "&")
      );
      keywordAllFieldReg = new RegExp(keywordAllField, "i");

      order = await Order.find({
        $or: [
          { username: keywordAllFieldReg },
          { firstName: keywordAllFieldReg },
          { lastName: keywordAllFieldReg },
          { email: keywordAllFieldReg },
          { company: keywordAllFieldReg },
          { phone: keywordAllFieldReg },
          { email: keywordAllFieldReg },
          { country: keywordAllFieldReg },
          { address: keywordAllFieldReg },
          { postcode: keywordAllFieldReg },
        ],
      });
    } else {
      order = await Order.find(query).sort({ [sortName]: sort });
    }

    if (order.length === 0)
      return res.status(400).json({
        success: false,
        message: "Không có đơn đặt hàng nào",
        data: {},
      });

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        category: order.reverse().slice(start, end),
        totalPages: Math.ceil(order.length / perPage),
        totalOrders: order.length,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.changeStatus = async (req, res) => {
  const { id } = req.params;
  const { status } = req.query;

  try {
    const order = await Order.findById(id);
    const products = order.cart;
    const prevStatus = order.status;

    if (!order) {
      return res
        .status(400)
        .json({ success: false, message: "Không thể cập nhật trạng thái" });
    }

    if (prevStatus === "pending" && status === "verified") {
      for (let i = 0; i < products.info.length; i++) {
        const product = await Product.findById(
          products.info[i].productId
        ).select("SKU amount");

        if (product.amount - products.info[i].quantity < 0) {
          return res.status(400).json({
            success: false,
            message: "Có sản phẩm đã hết hàng",
          });
        } else {
          await Product.findByIdAndUpdate(products.info[i].productId, {
            $inc: { amount: -1 * products.info[i].quantity },
          });
        }

        if (Object.keys(product).length === 0) continue;

        const bestSeller = await BestSeller.findOne({ SKU: product.SKU });
        if (!bestSeller) {
          const newBestSeller = new BestSeller({
            SKU: product.SKU,
            quantity: products.info[i].quantity,
          });
          newBestSeller.save();
        } else {
          await BestSeller.findOneAndUpdate(
            { SKU: product.SKU },
            { quantity: bestSeller.quantity + products.info[i].quantity }
          );
        }
      }
    }

    if (
      (prevStatus === "verified" && status === "pending") ||
      (prevStatus === "verified" && status === "cancelled")
    ) {
      for (let i = 0; i < products.info.length; i++) {
        const product = await Product.findById(
          products.info[i].productId
        ).select("SKU");

        await Product.findByIdAndUpdate(products.info[i].productId, {
          $inc: { amount: products.info[i].quantity },
        });

        if (Object.keys(product).length === 0) continue;

        const bestSeller = await BestSeller.findOne({ SKU: product.SKU });
        if (!bestSeller) continue;
        await BestSeller.findOneAndUpdate(
          { SKU: product.SKU },
          { quantity: bestSeller.quantity - products.info[i].quantity }
        );
      }
    }

    await Order.findByIdAndUpdate(id, { status }, { new: true });

    const statistics = await Statistics.find({});
    const statisticsId = statistics[0]._id;

    let message = "";
    switch (status) {
      case "pending":
        if (prevStatus === "verified") {
          await Statistics.findByIdAndUpdate(statisticsId, {
            $inc: { totalSalesMoney: -1 * products.totalPrice },
          });
        }
        message = "Đơn hàng đã chuyển sang trạng thái chờ!";
        break;
      case "verified":
        await Order.findByIdAndUpdate(id, {
          $set: {
            "cart.saleDate": moment().utcOffset("+07:00").format("DD-MM-YYYY"),
          },
        });
        if (prevStatus === "pending") {
          await Statistics.findByIdAndUpdate(statisticsId, {
            $inc: { totalSalesMoney: products.totalPrice },
          });
        }
        message = "Đơn hàng đã được duyệt";
        break;
      case "cancelled":
        if (prevStatus === "verified") {
          await Statistics.findByIdAndUpdate(statisticsId, {
            $inc: { totalSalesMoney: -1 * products.totalPrice },
          });
        }
        message = "Đơn hàng đã bị hủy";
        break;
      default:
        message = "";
    }

    return res.status(200).json({
      success: true,
      message,
      data: order,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.delete = async (req, res) => {
  try {
    const order = await Order.findByIdAndDelete({ _id: req.params.id });
    if (!order) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy khách hàng" });
    }

    return res.status(200).json({
      success: true,
      message: `Xóa đơn hàng của ${order.email} thành công`,
      data: order,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    const listOrder = await Order.deleteMany();
    if (listOrder.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "Không có đơn đặt hàng nào" });
    }

    return res.status(200).json({
      success: true,
      message: "Xóa thành công",
      data: listOrder,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.fake = async (req, res) => {
  try {
    const orders = await Order.find({});
    for (let order of orders) {
      await Order.findByIdAndUpdate(order._id, {
        $set: {
          "cart.orderDate": moment(order.cart.time).format("DD-MM-YYYY"),
        },
      });
    }
    return res.status(200).json({ success: true, message: "Fake success" });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
