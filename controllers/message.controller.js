const Message = require("../models/message.model");

exports.getAll = async (req, res) => {
  try {
    const messages = await Message.find({});
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: messages,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    const messages = await Message.deleteMany();
    if (messages.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "Không có message nào" });
    }

    return res.status(200).json({
      success: true,
      message: "Xóa message thành công",
      data: messages,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
