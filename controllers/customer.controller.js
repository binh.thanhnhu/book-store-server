require("dotenv").config();

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Customer = require("../models/customer.model");
const Statistics = require("../models/statistics.model");

const saltRounds = parseInt(process.env.SALT_ROUNDS_BCRYPT) || 10;
const secretToken = process.env.SECRET_TOKEN;

exports.register = async (req, res) => {
  const { username, password, email } = req.body;

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: "Thiếu tên tài khoản hoặc mật khẩu" });
  }

  try {
    const customerWithUsername = await Customer.find({ username });
    const customerWithEmail = await Customer.find({ email });

    if (customerWithUsername.length > 0) {
      return res
        .status(400)
        .json({ success: false, message: "Tên tài khoản đã tồn tại" });
    }
    if (customerWithEmail.length > 0) {
      return res
        .status(400)
        .json({ success: false, message: "Email đã tồn tại" });
    }

    const salt = await bcrypt.genSalt(saltRounds);
    const hashedPassword = await bcrypt.hash(password, salt);
    const newCustomer = new Customer({ ...req.body, password: hashedPassword });
    await newCustomer.save();

    const token = jwt.sign({ customerId: newCustomer._id }, secretToken);

    const statistics = await Statistics.find({});
    const statisticsId = statistics[0]._id;
    await Statistics.findByIdAndUpdate(
      statisticsId,
      { $inc: { totalCustomers: 1 } },
      { new: true }
    );

    return res.status(200).json({
      success: true,
      message: `Đăng kí tài khoản ${newCustomer.username} thành công!`,
      token,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.login = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: "Thiếu tên tài khoản hoặc mật khẩu" });
  }

  try {
    const customer = await Customer.findOne({ username });
    if (!customer) {
      return res
        .status(400)
        .json({ success: false, message: "Sai tài khoản hoặc mật khẩu" });
    }

    const validPassword = await bcrypt.compare(password, customer.password);
    if (!validPassword) {
      return res
        .status(400)
        .json({ success: false, message: "Sai tài khoản hoặc mật khẩu" });
    }

    const token = jwt.sign({ customerId: customer._id }, secretToken);

    return res.status(200).json({
      success: true,
      message: "Đăng nhập thành công!",
      token,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.logout = (req, res) => {
  return res.status(200).json({ success: true, message: "Bạn đã đăng xuất" });
};

exports.getSelf = async (req, res) => {
  try {
    const customerId = req.user?.customerId;

    const customer = await Customer.findById(customerId).select("-password");
    if (!customer) {
      return res.status(400).json({
        success: false,
        message: "Đường dẫn sai",
        data: customer,
      });
    }
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: customer,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getById = async (req, res) => {
  try {
    const customer = await Customer.findById(req.params.id);
    if (!customer) {
      return res.status(400).json({
        success: false,
        message: "Đường dẫn sai",
        data: customer,
      });
    }
    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        username: customer.username,
        firstName: customer.firstName,
        lastName: customer.lastName,
        company: customer.company,
        country: customer.country,
        address: customer.address,
        postcode: customer.postcode,
        phone: customer.phone,
        email: customer.email,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.getByOption = async (req, res) => {
  let { sort, sortName, keyword, keywordAllField } = req.query;
  let keywordReg, keywordAllFieldReg;
  if (keyword) keywordReg = new RegExp(keyword, "i");

  const page = parseInt(req.query.page) || 1;
  const perPage = parseInt(req.query.perPage) || 12;
  const start = (page - 1) * perPage;
  const end = page * perPage;

  try {
    let obj = { keywordReg };
    let query = {};
    let customer = [];
    for (let key in obj) {
      if (obj[key]) query[key] = obj[key];
    }
    console.log(query);

    if (keywordAllField) {
      keywordAllField = decodeURIComponent(
        keywordAllField.replace(/%jsfei026/g, "&")
      );
      keywordAllFieldReg = new RegExp(keywordAllField, "i");

      customer = await Customer.find({
        $or: [
          { username: keywordAllFieldReg },
          { firstName: keywordAllFieldReg },
          { lastName: keywordAllFieldReg },
          { email: keywordAllFieldReg },
          { company: keywordAllFieldReg },
          { phone: keywordAllFieldReg },
          { email: keywordAllFieldReg },
          { country: keywordAllFieldReg },
          { address: keywordAllFieldReg },
          { postcode: keywordAllFieldReg },
        ],
      });
    } else {
      customer = await Customer.find(query)
        .sort({ [sortName]: sort })
        .select("-password");
    }

    if (customer.length === 0)
      return res.status(400).json({
        success: false,
        message: "Không có khách hàng nào",
        data: {},
      });

    return res.status(200).json({
      success: true,
      message: "Lấy thông tin thành công",
      data: {
        category: customer.reverse().slice(start, end),
        totalPages: Math.ceil(customer.length / perPage),
        totalCustomers: customer.length,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.updateSelf = async (req, res) => {
  const { email } = req.body;
  const customerId = req.user?.customerId;

  delete req.body.username;
  delete req.body.password;

  if (!email) {
    return res.status(400).json({ success: false, message: "Phải có email" });
  }

  try {
    if (email) {
      const customer = await Customer.find({
        email,
        _id: { $ne: customerId },
      });
      if (customer.length > 0) {
        return res
          .status(400)
          .json({ success: false, message: "Email đã được dùng" });
      }
    }

    const customer = await Customer.findOneAndUpdate(
      { _id: customerId },
      req.body,
      {
        new: true,
      }
    ).select("-password");
    if (!customer) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy khách hàng" });
    }

    return res.status(200).json({
      success: true,
      message: `Cập nhật thành công`,
      data: customer,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.update = async (req, res) => {
  const { email } = req.body;

  delete req.body.username;
  delete req.body.password;

  if (!email) {
    return res.status(400).json({ success: false, message: "Phải có email" });
  }

  try {
    if (email) {
      const customer = await Customer.find({
        email,
        _id: { $ne: req.params.id },
      });
      if (customer.length > 0) {
        return res
          .status(400)
          .json({ success: false, message: "Email đã được dùng" });
      }
    }

    const customer = await Customer.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      {
        new: true,
      }
    ).select("-password");
    if (!customer) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy khách hàng" });
    }

    return res.status(200).json({
      success: true,
      message: `Cập nhật khách hàng ${customer.username} thành công`,
      data: customer,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.delete = async (req, res) => {
  try {
    const customer = await Customer.findByIdAndDelete({ _id: req.params.id });
    if (!customer) {
      return res
        .status(400)
        .json({ success: false, message: "Không tìm thấy khách hàng" });
    }

    const statistics = await Statistics.find({});
    const statisticsId = statistics[0]._id;
    await Statistics.findByIdAndUpdate(statisticsId, {
      $inc: { totalCustomers: -1 },
    });

    return res.status(200).json({
      success: true,
      message: `Xóa khách hàng ${customer.username} thành công`,
      data: customer,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    const customer = await Customer.deleteMany();
    if (customer.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "Không có khách hàng nào" });
    }

    const statistics = await Statistics.find({});
    const statisticsId = statistics[0]._id;
    await Statistics.findByIdAndUpdate(statisticsId, {
      $set: { totalCustomers: 0 },
    });

    return res.status(200).json({
      success: true,
      message: "Xóa tất cả khách hàng thành công",
      data: customer,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};
