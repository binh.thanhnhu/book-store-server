const express = require("express");
const router = express.Router();

const productController = require("../controllers/product.controller");
const { requireAuth, checkRole } = require("../middlewares/auth.middleware");

router.get("/get-by-id/:id", productController.getById);

router.get("/option", productController.getByOption);

router.get("/category", productController.getByCategory);

router.get("/", productController.getAll);

router.post(
  "/create",
  requireAuth,
  checkRole(["admin", "staff"]),
  productController.create
);

router.post(
  "/create-many",
  requireAuth,
  checkRole(["admin"]),
  productController.createMany
);

router.put(
  "/update/:id",
  requireAuth,
  checkRole(["admin", "staff"]),
  productController.update
);

router.delete(
  "/delete/:id",
  requireAuth,
  checkRole(["admin", "staff"]),
  productController.delete
);

router.delete(
  "/delete-all",
  requireAuth,
  checkRole(["admin"]),
  productController.deleteAll
);

module.exports = router;
