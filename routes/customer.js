const express = require("express");
const router = express.Router();

const customerController = require("../controllers/customer.controller");
const { requireAuth, checkRole } = require("../middlewares/auth.middleware");

router.post("/register", customerController.register);

router.post("/login", customerController.login);

router.post("/logout", customerController.logout);

router.put("/update", requireAuth, customerController.updateSelf);

router.put(
  "/update/:id",
  requireAuth,
  checkRole(["admin"]),
  customerController.update
);

router.get("/get-info", requireAuth, customerController.getSelf);

router.get("/get-by-id/:id", customerController.getById);

router.get("/option", customerController.getByOption);

router.delete(
  "/delete/:id",
  requireAuth,
  checkRole(["admin"]),
  customerController.delete
);

router.delete(
  "/deleteAll",
  requireAuth,
  checkRole(["admin"]),
  customerController.deleteAll
);

module.exports = router;
