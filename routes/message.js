const express = require("express");
const router = express.Router();

const messageController = require("../controllers/message.controller");
const { checkRole, requireAuth } = require("../middlewares/auth.middleware");

router.get(
  "/getAll",
  requireAuth,
  checkRole(["admin", "staff"]),
  messageController.getAll
);

router.delete(
  "/delete-all",
  requireAuth,
  checkRole(["admin", "staff"]),
  messageController.deleteAll
);

module.exports = router;
