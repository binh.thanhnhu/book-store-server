const express = require("express");
const router = express.Router();

const statisticsController = require("../controllers/statistics.controller");
const { checkRole, requireAuth } = require("../middlewares/auth.middleware");

router.get(
  "/getAll",
  requireAuth,
  checkRole(["admin", "staff"]),
  statisticsController.getAll
);

router.get(
  "/getByOption",
  requireAuth,
  checkRole(["admin", "staff"]),
  statisticsController.getByOption
);

router.post("/increaseSiteVisit", statisticsController.increaseSiteVisit);

router.get(
  "/getSalesOfWeek",
  requireAuth,
  checkRole(["admin", "staff"]),
  statisticsController.getSalesOfWeek
);

router.get(
  "/getOrdersOfWeek",
  requireAuth,
  checkRole(["admin", "staff"]),
  statisticsController.getOrdersOfWeek
);

router.get(
  "/init",
  requireAuth,
  checkRole(["admin"]),
  statisticsController.init
);

router.delete(
  "/delete-all",
  requireAuth,
  checkRole(["admin", "staff"]),
  statisticsController.deleteAll
);

module.exports = router;
