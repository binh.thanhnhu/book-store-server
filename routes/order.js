const express = require("express");
const router = express.Router();

const orderController = require("../controllers/order.controller");
const { requireAuth, checkRole } = require("../middlewares/auth.middleware");

router.post("/checkout", requireAuth, orderController.checkout);

router.get("/order-history", requireAuth, orderController.orderHistorySelf);

router.get("/get-all", orderController.getAll);

router.get("/get-by-id/:id", orderController.getById);

router.get("/option", orderController.getByOption);

router.get(
  "/change-status/:id/q",
  requireAuth,
  checkRole(["admin", "staff"]),
  orderController.changeStatus
);

router.delete(
  "/delete/:id",
  requireAuth,
  checkRole(["admin"]),
  orderController.delete
);

router.delete(
  "/delete-all",
  requireAuth,
  checkRole(["admin"]),
  orderController.deleteAll
);

router.get("/fake", requireAuth, checkRole(["admin"]), orderController.fake);

module.exports = router;
