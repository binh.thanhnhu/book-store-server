const express = require("express");
const router = express.Router();

const { requireAuth, checkRole } = require("../middlewares/auth.middleware");
const userController = require("../controllers/user.controller");

router.post(
  "/register",
  requireAuth,
  checkRole(["admin"]),
  userController.register
);

router.post("/login", userController.login);

router.post("/logout", userController.logout);

router.put("/update", userController.updateSelf);

router.put(
  "/update/:id",
  requireAuth,
  checkRole(["admin"]),
  userController.update
);

router.get("/get-info", userController.getUserSelf);

router.get("/get-by-id/:id", userController.getById);

router.get("/option", userController.getByOption);

router.delete(
  "/delete/:id",
  requireAuth,
  checkRole(["admin"]),
  userController.delete
);

router.delete("/delete-all", userController.deleteAll);

module.exports = router;
