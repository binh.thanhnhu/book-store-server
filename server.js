const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

require("dotenv").config();

// router
const productRouter = require("./routes/product");
const customerRouter = require("./routes/customer");
const orderRouter = require("./routes/order");
const userRouter = require("./routes/user");
const messageRouter = require("./routes/message");
const statisticsRouter = require("./routes/statistics");

// middlewares
const authMiddleware = require("./middlewares/auth.middleware");

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.MONGODB_URL, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log("MongoDB connected");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

connectDB();

const app = express();

app.use(express.json({ limit: "10mb" }));
app.use(cors());

app.use(logger("dev"));

app.get("/", (req, res) => res.send("Welcome! binhnt"));

app.use("/api/product", productRouter);
app.use("/api/customer", customerRouter);
app.use("/api/order", orderRouter);
app.use("/api/user", userRouter);
app.use("/api/message", messageRouter);
app.use("/api/statistics", statisticsRouter);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server is connected on port ${PORT}`);
});
