const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SiteVisitSchema = new Schema({
  date: {
    type: String,
    require: true,
  },
  quantity: {
    type: Number,
    require: true,
    default: 0,
  },
});

module.exports = mongoose.model("siteVisit", SiteVisitSchema);
