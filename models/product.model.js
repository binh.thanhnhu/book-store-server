const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductScheme = new Schema({
  type: {
    type: String,
    required: true,
    enum: ["VietNameseBook", "ForeignBook", "Stationery", "Souvenir", "Ebook"],
  },
  option: {
    type: String,
    enum: [
      "Literature",
      "Economic",
      "ChildrensBooks",
      "LifeSkill",
      "Textbook",
      "Comic",
      "ReligionAndSpirituality",
      "Journal",
      //
      "Cookbooks",
      "ScienceAndTechnology",
      "ParentingAndRelationships",
      "Magazines",
      "Dictionary",
      "SelfHelp",
      "ArtAndPhotography",
    ],
  },
  title: {
    type: String,
    required: true,
  },
  imageFromUrl: [
    {
      public_id: String,
      url: String,
      fileSize: Number,
    },
  ],
  price: {
    type: Number,
    required: true,
  },
  oldPrice: {
    type: Number,
  },
  rated: {
    type: Number,
  },
  author: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  publishingCompany: {
    type: String,
  },
  SKU: {
    type: String,
    unique: true,
    sparse: true,
  },
  numberOfPages: {
    type: Number,
  },
  size: {
    type: String,
  },
  dateOfPublication: {
    type: Date,
  },
  description: {
    type: String,
  },
  description2: {
    type: String,
  },
  customerReviews: {
    type: String,
  },
  creatorId: {
    type: String,
  },
  tags: [
    {
      type: String,
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const Ad = mongoose.model("products", ProductScheme);
module.exports = Ad;
