const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const StatisticsSchema = new Schema({
  totalSales: {
    type: Number,
    default: 0,
  },
  totalSalesMoney: {
    type: Number,
    default: 0,
  },
  totalCustomers: {
    type: Number,
    default: 0,
  },
  totalOrders: {
    type: Number,
    default: 0,
  },
  totalVisitsSite: {
    type: Number,
    default: 0,
  },
  totalVietnameseBooks: {
    type: Number,
    default: 0,
  },
  totalForeignBooks: {
    type: Number,
    default: 0,
  },
  totalStationeries: {
    type: Number,
    default: 0,
  },
  totalSouvenirs: {
    type: Number,
    default: 0,
  },
  totalEbooks: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model("statistics", StatisticsSchema);
