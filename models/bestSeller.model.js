const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BestSellerSchema = new Schema({
  SKU: {
    type: String,
    unique: true,
    require: true,
  },
  quantity: {
    type: Number,
  },
});

module.exports = mongoose.model("bestSeller", BestSellerSchema);
