const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  message: {
    type: String,
  },
  option: {
    type: String,
    enum: ["success", "warning", "error", "info"],
  },
  time: {
    type: Date,
    default: Date.now,
  },
  seen: {
    type: Boolean,
    default: false,
  },
  route: {
    type: String,
    enum: ["dashboard", "user", "customer", "product", "order"],
  },
  suffix: {
    type: String,
  },
});

module.exports = mongoose.model("message", MessageSchema);
