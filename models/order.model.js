const moment = require("moment");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  customerId: {
    type: String,
  },
  username: {
    type: String,
  },
  cart: {
    info: [
      {
        productId: String,
        productTitle: String,
        price: Number,
        quantity: Number,
      },
    ],
    totalPrice: {
      type: Number,
    },
    time: {
      type: Date,
      default: Date.now,
    },
    orderDate: {
      type: String,
      default: function () {
        return moment(this.cart.time).utcOffset("+07:00").format("DD-MM-YYYY");
      },
    },
    saleDate: {
      type: String,
      default: function () {
        return moment(this.cart.time).utcOffset("+07:00").format("DD-MM-YYYY");
      },
    },
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  company: {
    type: String,
  },
  country: {
    type: String,
  },
  address: {
    type: String,
  },
  postcode: {
    type: String,
  },
  phone: {
    type: String,
  },
  orderComments: {
    type: String,
  },
  email: {
    type: String,
  },
  status: {
    type: String,
    enum: ["pending", "verified", "cancelled"],
    default: "pending",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("orders", OrderSchema);
